﻿using UnityEngine;
using System.Collections;

public class ReduceHeight : MonoBehaviour
{
    private Transform obj;
    public float divideHeightBy = 100;

    private void Update()
    {
        obj.position = new Vector3(obj.localPosition.x, obj.localPosition.y / divideHeightBy, obj.localPosition.z);
    }
}
