﻿using UnityEngine;
using System.Collections;

public class Interact : MonoBehaviour {

	enum doorColour {RED, BLUE};
	private doorColour openDoor = doorColour.RED;
	[SerializeField] private GameObject RedDoors;
	[SerializeField] private GameObject BlueDoors;
	private Vector3 startPos;
	[SerializeField] private int needPickups = 1;
	public float pickupDistance = 15f;
	private int storyPickups = 0;
	private PlayerOxygen plox;
	[SerializeField] private Light redLight;
	[SerializeField] private Light blueLight;

    [SerializeField] private AudioClip oxygenSound;
    [SerializeField] private AudioClip storyPickupSound;
    [SerializeField] private AudioClip buttonSound;

	// Use this for initialization
	void Awake () {

		RedDoors.BroadcastMessage ("open");
		plox = GetComponentInParent<PlayerOxygen> ();
		startPos = transform.position;
	
	}

	void Start () {

		RedDoors.BroadcastMessage ("open");
		plox = GetComponentInParent<PlayerOxygen> ();
		startPos = transform.position;
		redLight.intensity = 3;
		blueLight.intensity = 0.5f;

	}

	private float TakeOxygen(OxyPickup tank)
	{
		float o = tank.refill(plox.currentOxygen, plox.maxOxygen);
		return o;
	}
	// Update is called once per frame

	private void PlayerKilled()
	{
		this.enabled = false;
	}

	private void Win()
	{
		this.enabled = false;
	}

	void Update () {

		if (Input.GetKeyDown(KeyCode.E))
		{
			print ("action");
			RaycastHit hit;
			if (Physics.Raycast (transform.position, transform.forward, out hit, pickupDistance))
			{
				print (hit.collider.tag);
				switch (hit.collider.tag) {

				case "OxyTank":
					{
                            GameObject.FindWithTag("OxygenSlider").GetComponent<AudioSource>().PlayOneShot(oxygenSound);
						plox.currentOxygen += TakeOxygen(hit.collider.GetComponent<OxyPickup>());
						break;
					}
				case "Switch":
					{
                            hit.collider.gameObject.GetComponent<AudioSource>().PlayOneShot(buttonSound);
						switch (openDoor) {

						case doorColour.RED:
							{
								openDoor = doorColour.BLUE;
								RedDoors.BroadcastMessage ("close");
								BlueDoors.BroadcastMessage ("open");
								//BlueDoors.SetActive(false);
								//RedDoors.SetActive(true);
								redLight.intensity = 0.5f;
								blueLight.intensity = 3f;
								break;
							}
						case doorColour.BLUE:
							{
								openDoor = doorColour.RED;
								BlueDoors.BroadcastMessage ("close");
								RedDoors.BroadcastMessage ("open");
								redLight.intensity = 3;
								blueLight.intensity = 0.5f;
								break;
							}

						}

						break;
					}
				case "Pickup":
					{
                            GameObject.FindWithTag("OxygenSlider").GetComponent<AudioSource>().PlayOneShot(storyPickupSound);
						storyPickups += 1;
                            hit.collider.gameObject.GetComponent<AudioSource>().PlayOneShot(oxygenSound);
						GetComponent<CamGUI> ().Pickup (hit.collider.transform.parent.gameObject.GetComponentInParent<StoryPickup>());
						Destroy (hit.collider.transform.parent.gameObject);
						GameObject.FindGameObjectWithTag ("Robot").GetComponent<NavMeshAI> ().chase = true;
						break;
					}

				case "Goal":
					{
						if ((storyPickups >= needPickups)) {
							BroadcastMessage ("Win");
							SendMessageUpwards ("Win");
						}
						break;
					}

				}

				//Mathf.Clamp(currentOxygen, 0, maxOxygen);
			}
		}
	
	}
}
