﻿using UnityEngine;
using System.Collections;

public class StunGun : MonoBehaviour {
    #region Variables
    [Header("Components")]
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private Transform bulletSpawn;
    private ControllerInput joystick;
    private AudioSource soundSource;
    [SerializeField] private AudioClip shotSound;

    [Header("Properties")]
    [SerializeField] [Range(0, 100)] private float rechargeTime = 1;
    private bool recharging = false;
    private float currentRechargeTime = 0;
    #endregion

    #region Getters/Setters
    public GameObject BulletPrefab
    {
        get { return bulletPrefab; }
    }
    public Transform BulletSpawn
    {
        get { return bulletSpawn; }
    }
    public float RechargeTime
    {
        get { return rechargeTime; }
    }
    public bool IsRecharging
    {
        get { return recharging; }
    }
    public float CurrentRechargeTime
    {
        get { return currentRechargeTime; }
    }
    #endregion

    #region Facilitators
    private void Shoot()
    {
        if (!recharging)
        {
            soundSource.PlayOneShot(shotSound);
            //GameObject bullet = (GameObject)Instantiate(bulletPrefab, bulletSpawn.position, Quaternion.LookRotation(transform.forward, transform.up), gameObject.transform);
			GameObject bullet = (GameObject)Instantiate(bulletPrefab, bulletSpawn.position, transform.parent.rotation, null);
            //bullet.transform.SetParent(null);
            recharging = true;
        }
    }
    #endregion

    #region MonoBehaviours
    private void Awake()
    {
        joystick = GameObject.FindGameObjectWithTag("Player").GetComponent<ControllerInput>();
        soundSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
		if (joystick.LeftTrigger.State == ButtonState.Press) {
			Shoot ();
			//print ("Shot.");
		}
		if (recharging) {
			currentRechargeTime += Time.deltaTime;
			if (currentRechargeTime >= rechargeTime) {
				recharging = false;
				currentRechargeTime = 0;
			}

		}
    }
    #endregion

	private void PlayerKilled()
	{
		this.enabled = false;
	}

	private void Win()
	{
		this.enabled = false;
	}
}
