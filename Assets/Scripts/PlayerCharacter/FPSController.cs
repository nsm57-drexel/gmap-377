﻿using UnityEngine;

/// <summary>
/// A MonoBehavior script for controlling a first-person character.
/// </summary>
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(ControllerInput))]
public class FPSController : MonoBehaviour
{
    #region Variables
    [Header("Components")] 
    private Rigidbody rb;
    private ControllerInput joystick;
    private AudioSource controllerAudioSource;

    [Header("Properties")]
    [SerializeField] private PlayerMovement movement = new PlayerMovement();
    [SerializeField] private FPLook looking = new FPLook();
    [SerializeField] private HeadBobbing bobbingEffect = new HeadBobbing();
    private AudioSource oxySliderSoundSource;
    [SerializeField] private AudioClip gameOverSound;
    private bool footstepSoundIsPlaying = false;

    [Header("Values")]
    private Vector2 moveInput = Vector2.zero;
    private float cameraLocalHeight;
    #endregion

    #region Getters/Setters
    #region Components
    public AudioSource ControllerAudioSource
    {
        get { return controllerAudioSource; }
    }
    public ControllerInput Joystick
    {
        get { return joystick; }
    }
    #endregion
    #region Movement (Instantiated Class)
    public PlayerMovement Movement
    {
        get { return movement; }
    }
    public CapsuleCollider CharacterCollider
    {
        get { return movement.collider; }
        private set { movement.collider = value; }
    }
    public Rigidbody RBody
    {
        get { return movement.rb; }
        private set { movement.rb = value; }
    }
    public float ForwardSpeed
    {
        get { return movement.forwardSpeed; }
        private set { movement.forwardSpeed = value; }
    }
    public float StrafeSpeed
    {
        get { return movement.strafeSpeed; }
        private set { movement.strafeSpeed = value; }
    }
    public float BackwardSpeed
    {
        get { return movement.backwardSpeed; }
        private set { movement.backwardSpeed = value; }
    }
    public bool AllowSprint
    {
        get { return movement.allowSprint; }
        private set { movement.allowSprint = value; }
    }
    public float SprintMultiplier
    {
        get { return movement.sprintMultiplier; }
        private set { movement.sprintMultiplier = value; }
    }
    public bool IsSprinting
    {
        get { return movement.sprinting; }
        private set { movement.sprinting = value; }
    }
    public float TargetSpeed
    {
        get { return movement.targetSpeed; }
        private set { movement.targetSpeed = value; }
    }
    public float TurnSpeed
    {
        get { return movement.turnSpeed; }
        private set { movement.turnSpeed = value; }
    }
    public bool AllowJumping
    {
        get { return movement.allowJumping; }
        private set { movement.allowJumping = value; }
    }
    public bool PressedJump
    {
        get { return movement.jump; }
        private set { movement.jump = false; }
    }
    public float JumpForce
    {
        get { return movement.jumpForce; }
        private set { movement.jumpForce = value; }
    }
    public Vector3 MoveDirection
    {
        get { return movement.moveDirection; }
        private set { movement.moveDirection = value; }
    }
    public float ColliderShrink
    {
        get { return movement.colliderShrink; }
        private set { movement.colliderShrink = value; }
    }
    public AnimationCurve SlopeCurveModifier
    {
        get { return movement.slopeCurveModifier; }
        private set { movement.slopeCurveModifier = value; }
    }
    public Vector3 GroundContactNormal
    {
        get { return movement.groundContactNormal; }
    }
    public JumpState JumpingState
    {
        get { return movement.jumpingState; }
    }
    public float MaxStamina
    {
        get { return movement.maxStamina; }
        private set { movement.maxStamina = value; }
    }
    public float CurrentStamina
    {
        get { return movement.currentStamina; }
        private set { movement.currentStamina = value; }
    }
    public bool Exhausted
    {
        get { return movement.exhausted; }
        private set { movement.exhausted = value; }
    }
    public float SmoothTime
    {
        get { return movement.smoothTime; }
        private set { movement.smoothTime = value; }
    }
    public bool AirControl
    {
        get { return movement.airControl; }
        private set { movement.airControl = value; }
    }
    public float GroundCheckDistance
    {
        get { return movement.groundCheckDistance; }
        private set { movement.groundCheckDistance = value; }
    }
    public float StickToGroundHelper
    {
        get { return movement.stickToGroundHelper; }
        private set { movement.stickToGroundHelper = value; }
    }
    
    #endregion
    #region Looking (Instantiated Class)
    public FPLook LookSettings
    {
        get { return looking; }
        private set { looking = value; }
    }
    public Camera Cam
    {
        get { return looking.cam; }
        private set { looking.cam = value; }
    }
    public float LookSensativityX
    {
        get { return looking.sensativityX; }
        private set { looking.sensativityX = value; }
    }
    public float LookSensativityY
    {
        get { return looking.sensativityY; }
        private set { looking.sensativityY = value; }
    }
    public Vector2 LookSensativityVector
    {
        get { return new Vector2(looking.sensativityX, looking.sensativityY); }
    }
    public Vector2 LookDirection
    {
        get { return looking.direction; }
        private set { looking.direction = value; }
    }
    #endregion
    #region BobbingEffect (Instantiated Class)
    public HeadBobbing BobbingEffect
    {
        get { return bobbingEffect; }
    }
    public bool UseHeadBoob
    {
        get { return bobbingEffect.useHeadBob; }
        private set { bobbingEffect.useHeadBob = value; }
    }
    public AnimationCurve VerticalBobCurve
    {
        get { return bobbingEffect.horizontalBobCurve; }
        private set { bobbingEffect.verticalBobCurve = value; }
    }
    public AnimationCurve HorizontalBobCurve
    {
        get { return bobbingEffect.horizontalBobCurve; }
        private set { bobbingEffect.horizontalBobCurve = value; }
    }
    public float CurrentBobTime
    {
        get { return bobbingEffect.currentBobTime; }
        private set { bobbingEffect.currentBobTime = value; }
    }
    #endregion
    #region Values
    public Vector2 MoveInput
    {
        get { return moveInput; }
    }
    public float CameraStartingHeight
    {
        get { return cameraLocalHeight; }
    }
    #endregion
    #endregion

    #region Facilitators
    private void GetInput()
    {
        moveInput = joystick.LeftStick.GetInput + joystick.DPad.GetInput;
        moveInput.x = Mathf.Clamp(moveInput.x, -1, 1);
        moveInput.y = Mathf.Clamp(moveInput.y, -1, 1);
        MoveDirection = new Vector3(moveInput.x, 0, moveInput.y);
        LookDirection = joystick.RightStick.GetInput;
    }
    private void Move()
    {
        movement.Move();  
    }
    private void Look() { looking.Look(); }
    #endregion

    #region MonoBehaviors
    protected virtual void Awake()
    {
        // Own Components
        rb = GetComponent<Rigidbody>();
        joystick = GetComponent<ControllerInput>();
        controllerAudioSource = GetComponent<AudioSource>();

        // Movement Components
        movement.character = transform;
        CharacterCollider = GetComponent<CapsuleCollider>();
        RBody = GetComponent<Rigidbody>();

		// Controls
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;

        // FPSLook Components
        looking.character = transform;
        if (GetComponentsInChildren<Camera>().Length != 0)
            Cam = GetComponentsInChildren<Camera>()[0];
        else
        {
            Cam = Camera.main;
            Cam.transform.SetParent(transform);
        }

        oxySliderSoundSource = GameObject.FindWithTag("OxygenSlider").GetComponent<AudioSource>();
    }

    protected virtual void Start()
    {
        cameraLocalHeight = Cam.transform.localPosition.y;
    }

    protected virtual void FixedUpdate()
    {
		GetInput();
		Look();
		if (joystick.LeftStickPress.State == ButtonState.Hold)
            IsSprinting = true;
        else
            IsSprinting = false;
        Move();
        if (controllerAudioSource.enabled == false || rb.velocity == Vector3.zero)
        {
            controllerAudioSource.Stop();
            footstepSoundIsPlaying = false;
        }
        else if (!footstepSoundIsPlaying)
        {
            footstepSoundIsPlaying = true;
            controllerAudioSource.Play();
        }
    }
    #endregion

	private void PlayerKilled()
	{
        controllerAudioSource.Stop();
        controllerAudioSource.enabled = false;
        oxySliderSoundSource.PlayOneShot(gameOverSound);
		enabled = false;
	}

	public void onDeath(Vector3 Dir)
	{
		rb.AddForce ((250000 * Dir));
	}

	private void Win()
	{
		enabled = false;
	}
}