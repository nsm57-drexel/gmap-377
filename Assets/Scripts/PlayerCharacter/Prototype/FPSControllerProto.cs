﻿/*using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(CombinedInput))]
public class FPSControllerProto : MonoBehaviour
{
    //-----Variables-----
    [Header("Components")]
    private CharacterController character;
    private AudioSource soundSource;
    private Camera cam;
    private CombinedInput joystick;

    [Header("Movement")]
    [SerializeField] protected bool running = false;
    [SerializeField] protected float walkSpeed = 5;
    [SerializeField] protected float runMultiplier = 1.5f;
    protected Vector3 moveDirection = Vector3.zero;
    [SerializeField] protected float lookSpeed = 10;
    protected Vector2 lookDirection = Vector2.zero;

    [Header("Movement Effects")]
    [SerializeField] protected bool headBob = true;
    [SerializeField] protected AnimationCurve headBobCurve = new AnimationCurve();
    private float currentDistance = 0;
    private float currentReturnTime = 0;

    //-----Inspectors-----
    //Components
    public CharacterController GetCharacterController() { return character; }
    public AudioSource GetSoundSource() { return soundSource; }
    public Camera GetCamera() { return cam; }
    
    //Movement
    public bool IsRunning() { return running; }
    public float GetWalkSpeed() { return walkSpeed; }
    public float GetRunMultiplier() { return runMultiplier; }
    public float GetLookSpeed() { return lookSpeed; }

    //Movement Effects
    public bool HeadBobs() { return headBob; }
    public AnimationCurve GetHeadBobCurve() { return headBobCurve; }

    //-----Mutators-----
    //Movement
    public void SetWalkSpeed(float speed) { walkSpeed = speed; }
    public void SetRunMultiplier(float speed) { runMultiplier = speed; }
    public void SetLookSpeed(float speed) { lookSpeed = speed; }

    //Movement Effects
    public void SetHeadBob(bool bob) { headBob = bob; }
    public void SetHeadBobCurve(AnimationCurve curve) { headBobCurve = curve; }
    public void SetHeadBobCurve(float[] times, float[] values)
    {
        if (times.Length != values.Length)
        {
            Debug.LogError("There are not the same number of time variables as there are key variables. Curve remains unchanged.");
            return;
        }
        AnimationCurve temp = new AnimationCurve();
        for (int i = 0; i < times.Length; i++)
            temp.AddKey(times[i], values[i]);
        for (int i = 0; i < temp.keys.Length; i++)
            temp.SmoothTangents(i, 0);
        temp.postWrapMode = WrapMode.Loop;
        temp.preWrapMode = WrapMode.Loop;
        headBobCurve = temp;
    }
    
    //-----Facilitators-----
    protected float RestrictValue(float value, float min = -1, float max = 1)
    {
        if (max < min)
        {
            Debug.LogError("Max value is less than min value. Input value returned.");
            return value;
        }
        if (value > max)
            return max;
        else if (value < min)
            return min;
        return value;
    }

    protected void GetInput()
    {
        Vector2 temp = joystick.GetStick(Stick.Left).GetInput() + joystick.GetStick(Stick.Pad).GetInput();
        temp.x = RestrictValue(temp.x);
        temp.y = RestrictValue(temp.y);
        moveDirection = new Vector3(temp.x, 0, temp.y);

        if (joystick.GetButton(Button.LeftStickPress).GetValue() > 0)
            running = true;
        else
            running = false;

        lookDirection = joystick.GetStick(Stick.Right).GetInput();
    }

    protected void Move()
    {
        float temp = lookDirection.x;
        lookDirection.x = lookDirection.y;
        lookDirection.y = temp;
        lookDirection *= lookSpeed * Time.deltaTime;
        cam.transform.Rotate(new Vector3(lookDirection.x, 0, 0));
        transform.Rotate(new Vector3(0, lookDirection.y, 0));


        if (moveDirection != Vector3.zero)
        {
            currentReturnTime = 0;
            Vector3 lastPosition = transform.position;
            float currentSpeed = walkSpeed;
            if (running)
                currentSpeed *= runMultiplier;
            character.Move(transform.right * moveDirection.x * currentSpeed * Time.deltaTime);
            character.Move(transform.forward * moveDirection.z * currentSpeed * Time.deltaTime);
            Vector3 positionChange = transform.position - lastPosition;
            currentDistance += Mathf.Abs(Mathf.Sqrt(Mathf.Pow(positionChange.x, 2) + Mathf.Sqrt(Mathf.Pow(positionChange.z, 2))));
            if (currentDistance >= 3)
                currentDistance -= 3;
            else if (currentDistance <= 3)
                currentDistance += 3;
            cam.transform.localPosition = new Vector3(0, headBobCurve.Evaluate(currentDistance), 0);
        }
        else if (cam.transform.localPosition != new Vector3(0, 0.8f, 0))
        {
            currentReturnTime += Time.deltaTime * runMultiplier;
            cam.transform.localPosition = Vector3.Lerp(new Vector3(0, currentDistance, 0), new Vector3(0, 0.8f, 0), currentReturnTime);
        }
        else
        {
            currentDistance = 0;
            currentReturnTime = 0;
        }
        
        if (!Physics.Linecast(transform.localPosition, new Vector3(transform.localPosition.x, transform.localPosition.y - 0.1f, transform.localPosition.z)))
            character.Move(Vector3.up * Physics.gravity.y * Time.deltaTime);
    }

    protected void BobHead()
    {

    }

    //-----MonoBehaviors-----
    protected virtual void Start()
    {
        character = GetComponent<CharacterController>();
        joystick = GetComponent<CombinedInput>();
        soundSource = GetComponent<AudioSource>();
        cam = Camera.main;
    }

    protected virtual void Update()
    {
        GetInput();
        Move();
    }
}*/