﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class BulletTravel : MonoBehaviour
{
    //-----Variables-----
    [Header("Components")]
    private Rigidbody rb;

    [Header("Properties")]
    [SerializeField] protected float speed = 10;
    [SerializeField] protected float maxTime = 25;
    private float shotTimer = 0;

    //-----Inspectors-----
    public float GetSpeed() { return speed; }
    public float GetMaxDistance() { return maxTime; }
    public float GetShotTimer() { return shotTimer; }
    public float GetRemainingTimer() { return maxTime - shotTimer; }

    //-----Mutators-----
    public void SetSpeed(float speed) { this.speed = speed; }
    public void SetMaxDistance(float distance) { maxTime = distance; }

    //-----MonoBehaviors-----
    protected virtual void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    
    protected virtual void Update()
    {
        rb.velocity = transform.forward * speed;
        shotTimer += Time.deltaTime;
        if (shotTimer >= maxTime)
            Destroy(gameObject);
    }

    protected virtual void OnTriggerEnter(Collider collider)
    {
        if (collider.tag != "Player")
        {
            collider.gameObject.SendMessage("SetStunned", true, SendMessageOptions.DontRequireReceiver);
            Destroy(gameObject);
        }
    }
}