﻿using UnityEngine;
using System.Collections;

public class GunShotProto : MonoBehaviour
{
    //----Variables-----
    [Header("Spawning")]
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private Transform bulletSpawn;

    [Header("Gun Properties")]
    [SerializeField] protected float rechargeTime = 1;
    protected bool recharging = false;
    protected float currentRechargeTime = 0;

    //-----Inspectors-----
    public GameObject GetBulletPrefab() { return bulletPrefab; }
    public Transform GetBulletSpawn() { return bulletSpawn; }
    public float GetRechargeTime() { return rechargeTime; }
    public bool IsRecharging() { return recharging; }
    public float GetCurrentRechargeTime() { return currentRechargeTime; }
    public float GetRechargeTimeRemaining() { return rechargeTime - currentRechargeTime; }

    //-----Mutators-----
    public void SetRechargeTime(float time) { rechargeTime = time; }

    //-----Facilitators-----
    protected virtual void Shoot()
    {
        if (!recharging)
        {
            GameObject bullet = (GameObject)Instantiate(bulletPrefab, bulletSpawn.position, Quaternion.LookRotation(transform.forward, transform.up), gameObject.transform);
            bullet.transform.SetParent(null);
            recharging = true;
        }
    }

    //-----MonoBehaviors-----
    protected virtual void Update()
    {
        if (Input.GetButtonDown("Fire1"))
            Shoot();
        if (recharging)
        {
            currentRechargeTime += Time.deltaTime;
            if (currentRechargeTime >= rechargeTime)
            {
                recharging = false;
                currentRechargeTime = 0;
            }
        }
    }
}
