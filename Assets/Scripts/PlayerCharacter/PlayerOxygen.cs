﻿using UnityEngine;

public class PlayerOxygen : MonoBehaviour
{
    #region Variables
    [Header("Components")]
    private Camera cam;
    private ControllerInput joystick;

    [Header("Properties")]
    public float maxOxygen = 100;
    [HideInInspector] public float currentOxygen = 100;
	[SerializeField] private float baseDepletionRate = 1;
	[SerializeField] private float hotDeplationRate = 0.5f;
	private float depletionRate;
    private float depletionTimer = 0;
	private RaycastHit hit;
    #endregion

    #region Facilitators
   
    #endregion

    #region MonoBehaviors
    private void Awake()
    {
        cam = GetComponent<FPSController>().Cam;
        joystick = GetComponent<ControllerInput>();
		depletionRate = baseDepletionRate;
    }

    private void Start() { currentOxygen = maxOxygen; }

    private void Update()
    {
		Physics.Linecast (transform.position, (new Vector3 (transform.position.x, transform.position.y - 12f, transform.position.z)), out hit);
		if (hit.collider.tag == "FireRoom") {
			depletionRate = hotDeplationRate;
		} else {
			depletionRate = baseDepletionRate;
		}
		depletionTimer += Time.deltaTime;
        if (depletionTimer >= depletionRate)
        {
            currentOxygen -= 1;
            depletionTimer = 0;
        }
        if (currentOxygen <= 0)
            BroadcastMessage("PlayerKilled", SendMessageOptions.DontRequireReceiver);
		
    }
    #endregion
}
