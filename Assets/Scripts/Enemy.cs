﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
    //-----Variables-----
    [Header("Components")]
    private Rigidbody rb;
    
    [Header("Targeting")]
    private Transform target; //the enemy's target

    [Header("Movement")]
    [SerializeField] protected float moveSpeed = 3; //move speed
    [SerializeField] protected float rotationSpeed = 3; //speed of turning
    [SerializeField] protected float range = 25f;
    protected bool stunned = false;
    [SerializeField] protected float stunDuration = 5;
    protected float stunTimer = 0;

    //-----Inspectors-----
    //Components
    public Rigidbody GetRigidbody() { return rb; }

    //Targeting
    public Transform GetTarget() { return target; }

    //Movement
    public float GetMoveSpeed() { return moveSpeed; }
    public float GetRotationSpeed() { return rotationSpeed; }
    public float GetRange() { return range; }
    public bool IsStunned() { return stunned; }
    public float GetStunDuration() { return stunDuration; }
    public float GetStunTimer() { return stunTimer; }
    public float GetStunTimerRemaining() { return stunDuration - stunTimer; }

    //-----Mutators-----
    //Movement
    public void SetMoveSpeed(float speed) { moveSpeed = speed; }
    public void SetRotationSpeed(float speed) { rotationSpeed = speed; }
    public void SetRange(float range) { this.range = range; }
    public void SetStunned(bool isStunned)
    {
        stunned = isStunned;
        rb.velocity = Vector3.zero;
    }
    public void SetStunDuration(float time) { stunDuration = time; }
    
    //-----Facilitators-----
    protected void LookAtTarget()
    {
        Quaternion targetRotation = Quaternion.LookRotation(target.position - transform.position, Vector3.up);
        targetRotation = Quaternion.Euler(0, targetRotation.eulerAngles.y, 0);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
    }
    //protected void LookAtTarget() { transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(target.position - transform.position), rotationSpeed * Time.deltaTime); }

    //-----MonoBehaviors-----
    protected virtual void Start()
    {
        rb = GetComponent<Rigidbody>();
        target = GameObject.FindWithTag("Player").transform; //target the player
    }

    protected virtual void Update()
    {
        if (stunned)
        {
            rb.velocity = Vector3.zero;
            stunTimer += Time.deltaTime;
            if (stunTimer >= stunDuration)
                stunned = false;
        }
        else
        {
            stunTimer = 0;
            var distance = Vector3.Distance(transform.position, target.position);
            LookAtTarget();
            if (distance <= range && distance > 0)
                rb.velocity = transform.forward * moveSpeed;
                //transform.position += transform.forward * moveSpeed * Time.deltaTime;
        }
    }

    protected virtual void OnCollisionEnter(Collision collision)
    {
        if (!stunned)
            if (collision.gameObject.tag == "Player")
                collision.gameObject.SendMessage("Kill", SendMessageOptions.DontRequireReceiver);
    }
}