﻿using UnityEngine;

public class Doors : MonoBehaviour
{
    private AudioSource soundSource;
    [SerializeField] private AudioClip closeSound;
    [SerializeField] private AudioClip openSound;
    private float closeHeight = 0;
    private float currentHeight = 0;
    private float timing = 0;
	[SerializeField] private float speed = 10f;
    private bool isOpen = false;
	
    void Awake() { soundSource = GetComponent<AudioSource>(); }

    void Start() { closeHeight = transform.position.y; }

	// Update is called once per frame
	void Update ()
    {
        if (isOpen)
            timing += Time.deltaTime;
        else
            timing -= Time.deltaTime;
        timing = Mathf.Clamp01(timing);
        currentHeight = Mathf.Lerp(closeHeight, closeHeight + 5, timing * speed);
        transform.position = new Vector3(transform.position.x, currentHeight, transform.position.z);
        /*if (soundSource == null)
            Debug.Log(gameObject.name);*/
	}

	void open()
	{
        soundSource.PlayOneShot(openSound, 0.1f);
        isOpen = true;
	}

	void close()
	{
        soundSource.PlayOneShot(closeSound, 0.1f);
        isOpen = false;
	}

}