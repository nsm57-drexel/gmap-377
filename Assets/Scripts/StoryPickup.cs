﻿using UnityEngine;
using System.Collections;

public class StoryPickup : MonoBehaviour {

	[SerializeField] private Vector3 loc1 = new Vector3(), loc2 = new Vector3(), loc3 = new Vector3(), loc4 = new Vector3(), loc5 = new Vector3(), loc6 = new Vector3(), loc7 = new Vector3(), loc8 = new Vector3();
	public Sprite img;
	// Use this for initialization
	void Awake () {

		int place = Random.Range (1, 8);
		//print (place);
		switch (place)
		{
		case 1:
			{
				transform.position = new Vector3(loc1.x, loc1.y, loc1.z);
				break;
			}
		case 2:
			{
				transform.position = new Vector3(loc2.x, loc2.y, loc2.z);
				break;
			}
		case 3:
			{
				transform.position = new Vector3(loc3.x, loc3.y, loc3.z);
				break;
			}
		case 4:
			{
				transform.position = new Vector3(loc4.x, loc4.y, loc4.z);
				break;
			}
		case 5:
			{
				transform.position = new Vector3(loc5.x, loc5.y, loc5.z);
				break;
			}
		case 6:
			{
				transform.position = new Vector3(loc6.x, loc6.y, loc6.z);
				break;
			}
		case 7:
			{
				transform.position = new Vector3(loc7.x, loc7.y, loc7.z);
				break;
			}
		case 8:
			{
				transform.position = new Vector3(loc8.x, loc8.y, loc8.z);
				break;
			}
		}
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
