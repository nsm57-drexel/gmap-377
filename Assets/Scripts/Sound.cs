﻿using UnityEngine;
using System.Collections;

public class Sound : MonoBehaviour {

	[SerializeField] private AudioSource muzak;
	private NavMeshAI varPull;


	// Use this for initialization
	void Awake () {
		varPull = GetComponent<NavMeshAI> ();
	}
	
	// Update is called once per frame
	void Update () {

		muzak.volume = (1 - (Vector3.Distance (transform.position, varPull.GetTarget ().transform.position)/100));
		//print (1 - (Vector3.Distance (transform.position, varPull.GetTarget ().transform.position)/100));

		if (varPull.isChasing ()) {
			muzak.mute = true;
		} else {
			muzak.mute = false;
		}
	
	}
}
