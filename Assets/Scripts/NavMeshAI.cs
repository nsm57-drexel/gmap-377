using UnityEngine;
using System.Collections;

public class NavMeshAI : MonoBehaviour {

	protected NavMeshAgent agent;
	protected NavMeshHit hitNav;
	protected GameObject target;
	protected RaycastHit hitRay;
	protected RaycastHit[] hitSphere;
	[SerializeField] protected float stunDuration = 5;
	[SerializeField] protected float pathDuration = 2;
	[SerializeField] protected float memFlush = 5;
	[SerializeField] protected float detectionDistance = 1f;
	protected float memTimer = 0;
	protected float stunTimer = 0;
	protected float pathTimer = 0;
	protected bool stunned = false;
	public bool chase = false;
	[SerializeField] protected float roamMod = 40f; //modifier for randomized roaming
	[SerializeField] protected float moveSpeed = 3; //move speed
	[SerializeField] protected float range = 25f; //detection range for player
	private Rigidbody rb;

    private AudioSource soundSource;
    [SerializeField] private AudioClip stunSound;
    private bool stunSoundIsPlaying = false;

	//Copy/pasted methods from previous enemy script:
	//-----Inspectors-----
	//Components
	public Rigidbody GetRigidbody() { return rb; }

	//Targeting
	public GameObject GetTarget() { return target; }
	public bool isChasing() {
		return chase;
	}
	//Movement
	public float GetMoveSpeed() { return moveSpeed; }
	public float GetRange() { return range; }
	public bool IsStunned() { return stunned; }
	public float GetStunDuration() { return stunDuration; }
	public float GetStunTimer() { return stunTimer; }
	public float GetStunTimerRemaining() { return stunDuration - stunTimer; }

	//-----Mutators-----
	//Movement
	public void SetMoveSpeed(float speed) { moveSpeed = speed; }
	public void SetRange(float range) { this.range = range; }
	public void SetStunned(bool isStunned)
	{
        
		stunned = isStunned;
		rb.velocity = Vector3.zero;
		stunTimer = 0f;
		chase = false;
	}
	public void SetStunDuration(float time) { stunDuration = time; }

	protected virtual void OnCollisionEnter(Collision collision)
	{
		if (!stunned)
		    if (collision.gameObject.tag == "Player")
		    {
			    collision.gameObject.GetComponent<FPSController> ().onDeath (transform.forward);
			    target.BroadcastMessage ("PlayerKilled");
			    agent.Stop ();
		    }
		
	}

	//end imported methods

    void Awake()
    {
        soundSource = GetComponent<AudioSource>();
    }

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();
		target = GameObject.FindGameObjectWithTag ("Player");
		agent = GetComponent<NavMeshAgent> ();
		agent.speed = moveSpeed;
	}
	
	// Update is called once per frame
	void Update () {

		pathTimer += Time.deltaTime;
		if (stunned) {
            if (!stunSoundIsPlaying)
            {
                soundSource.PlayOneShot(stunSound);
                stunSoundIsPlaying = true;
            }
			stunTimer += Time.deltaTime;
			agent.destination = transform.position; //don't move if stunned
		} else if (chase) {
			//print ("RUN");
			Physics.Linecast (transform.position, target.transform.position, out hitRay);
			if (hitRay.collider.tag == "Player") {
				agent.destination = target.transform.position;
				memTimer = 0;
			} else {
				memTimer += Time.deltaTime;
				if (memTimer >= memFlush)
					chase = false;
			}
		}
		else {
			hitSphere = Physics.SphereCastAll(transform.position, detectionDistance, transform.forward, range);
			Physics.Linecast (transform.position, target.transform.position, out hitRay);
			foreach(RaycastHit hit in hitSphere)
			{
				if (hit.collider.tag == "Player" && hitRay.collider.tag == "Player") {
					agent.destination = target.transform.position;
					chase = true;
				}
			}
		}

		if (pathTimer >= pathDuration && !chase && !stunned) {
			pathTimer = 0;
			Vector3 rand = new Vector3 (Random.Range (-roamMod, roamMod), 0.0f, Random.Range (-roamMod, roamMod)); //randomized roaming deviation from player
			agent.destination = new Vector3 ((target.transform.position.x + rand.x), (target.transform.position.y + rand.y), (target.transform.position.z + rand.z)); //randomized roaming. Killbot will always be near player.
		}

		if (stunTimer >= stunDuration) {

			stunTimer = 0;
			stunned = false;
            stunSoundIsPlaying = false;
			pathTimer = pathDuration;

		}
	
	}
}
