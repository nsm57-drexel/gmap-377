﻿using System;
using UnityEngine;

public enum JumpState { Grounded, Jumping, Falling }

/// <summary>
/// Contains common movement parameters.
/// </summary>
[Serializable]
public class BasicMovement
{
    #region Variables
    [Header("Components")]
    [HideInInspector] public Transform character;
    [HideInInspector] public Rigidbody rb;
    [HideInInspector] public CapsuleCollider collider;

    [Header("Properties")]
    public float forwardSpeed = 8;
    public float strafeSpeed = 7;
    public float backwardSpeed = 4;
    public bool allowSprint = true;
    public float sprintMultiplier = 2;
    [HideInInspector] public bool sprinting = false;
    public float turnSpeed = 10;
    public float smoothTime = 20;
    [HideInInspector] public Vector3 moveDirection = Vector3.zero;
    [Tooltip("Set to 0.1 or more if you get stuck in a wall.")]
    public float colliderShrink = 0.1f;
    public AnimationCurve slopeCurveModifier = new AnimationCurve(new Keyframe(-90, 1), new Keyframe(0, 1), new Keyframe(90, 0));

    [Header("Jumping")]
    public bool allowJumping = false;
    [HideInInspector] public bool jump = false;
    public float jumpForce = 30;
    public bool airControl = false;
    [HideInInspector] public JumpState jumpingState = JumpState.Grounded;
    public float groundCheckDistance = 0.01f;
    public float stickToGroundHelper = 0.5f;
    [HideInInspector] public Vector3 groundContactNormal = Vector3.zero;

    [Header("Values")]
    [HideInInspector] public float targetSpeed = 0;
    private float previousSpeed = 0;
    private float currentSmooth = 0;
    private Vector3 localVelocity = Vector3.zero;
    private Vector3 previousDirection = Vector3.zero;
    #endregion

    #region Constructors
    public BasicMovement() { }
    public BasicMovement(BasicMovement movementSettings)
    {
        forwardSpeed = movementSettings.forwardSpeed;
        strafeSpeed = movementSettings.strafeSpeed;
        backwardSpeed = movementSettings.backwardSpeed;
        allowSprint = movementSettings.allowSprint;
        sprintMultiplier = movementSettings.sprintMultiplier;
        turnSpeed = movementSettings.turnSpeed;
    }
    public BasicMovement(float movementSpeed, float turnSpeed)
    {
        forwardSpeed = movementSpeed;
        strafeSpeed = movementSpeed;
        backwardSpeed = movementSpeed;
        allowSprint = false;
        sprintMultiplier = 1;
        this.turnSpeed = turnSpeed;
    }
    public BasicMovement(float movementSpeed, float sprintMultiplier, float turnSpeed)
    {
        forwardSpeed = movementSpeed;
        strafeSpeed = movementSpeed;
        backwardSpeed = movementSpeed;
        allowSprint = true;
        this.sprintMultiplier = sprintMultiplier;
        this.turnSpeed = turnSpeed;
    }
    public BasicMovement(float forwardSpeed, float strafeSpeed, float backwardSpeed, bool allowSprint, float sprintMultiplier, float turnSpeed)
    {
        this.forwardSpeed = forwardSpeed;
        this.strafeSpeed = strafeSpeed;
        this.backwardSpeed = backwardSpeed;
        this.allowSprint = allowSprint;
        this.sprintMultiplier = sprintMultiplier;
        this.turnSpeed = turnSpeed;
    }
    #endregion

    #region
    public void UpdateTargetSpeed()
    {
        if (moveDirection == Vector3.zero)
            targetSpeed = 0;
        else if (moveDirection.z > 0)
            targetSpeed = forwardSpeed;
        else if (moveDirection.z < 0)
            targetSpeed = backwardSpeed;
        else if (moveDirection.x > 0 || moveDirection.x < 0)
            targetSpeed = strafeSpeed;
        if (sprinting)
            targetSpeed *= sprintMultiplier;
    }

    public void Move()
    {
        UpdateTargetSpeed();
        GroundCheck();
        if (jumpingState == JumpState.Grounded || airControl)
        {
            if (moveDirection != Vector3.zero)
            {
                currentSmooth += Time.fixedDeltaTime;
                currentSmooth = Mathf.Clamp(currentSmooth, 0, smoothTime);
                localVelocity = character.transform.TransformDirection(moveDirection);
                localVelocity *= targetSpeed * (currentSmooth / smoothTime);
                previousDirection = moveDirection;
                previousSpeed = targetSpeed;
            }
            else
            {
                currentSmooth -= (Time.fixedDeltaTime * 20);
                currentSmooth = Mathf.Clamp(currentSmooth, 0, smoothTime);
                localVelocity = character.transform.TransformDirection(previousDirection);
                localVelocity *= previousSpeed * (currentSmooth / smoothTime);
            }
            rb.velocity = localVelocity;
        }
        if (jump)
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
    }

    //Parts taken from Unity's Standard Asset RigidbodyFirstPersonController.cs
    public void GroundCheck()
    {
        RaycastHit hitInfo = new RaycastHit();
        if (Physics.SphereCast(character.position, collider.radius * (1 - colliderShrink), Vector3.down, out hitInfo, collider.height / 2 + groundCheckDistance, Physics.AllLayers, QueryTriggerInteraction.Ignore))
        {
            jumpingState = JumpState.Grounded;
            groundContactNormal = hitInfo.normal;
        }
        else
            jumpingState = JumpState.Falling;
    }

    private float SlopeMultiplier()
    {
        float angle = Vector3.Angle(groundContactNormal, Vector3.up);
        return slopeCurveModifier.Evaluate(angle);
    }

    private void StickToGroundHelper()
    {
        RaycastHit hitInfo = new RaycastHit();
        if (Physics.SphereCast(character.position, collider.radius * (1 - colliderShrink), Vector3.down, out hitInfo, collider.height / 2 + groundCheckDistance, Physics.AllLayers, QueryTriggerInteraction.Ignore))
            if (Mathf.Abs(Vector3.Angle(hitInfo.normal, Vector3.up)) < 85)
                rb.velocity = Vector3.ProjectOnPlane(rb.velocity, hitInfo.normal);
    }
    #endregion
}

/// <summary>
/// Contains parameters for player-specific movement. Optimized for Rigidbody controllers.
/// </summary>
[Serializable]
public class PlayerMovement : BasicMovement
{
    #region Variables
    [Header("Stamina")]
    public float maxStamina = 100;
    public float currentStamina = 100;
    public bool exhausted = false;
    #endregion

    #region Constructors
    public PlayerMovement() { }
    public PlayerMovement(PlayerMovement movementSettings) : base(movementSettings) { }
    public PlayerMovement(BasicMovement basicSettings, float maxStamina = 100, float smoothTime = 20, bool controlWhileAirbourne = true) : base(basicSettings)
    {
        this.maxStamina = maxStamina;
        currentStamina = maxStamina;
        this.smoothTime = smoothTime;
        airControl = controlWhileAirbourne;
    }
    #endregion
}