﻿using System;
using UnityEngine;

/// <summary>
/// Contains parameters to manipulate a first-person camera to simulate a head bobbing effect.
/// </summary>
[Serializable]
public class HeadBobbing
{
    #region Variables
    public bool useHeadBob = true;
    public AnimationCurve verticalBobCurve;
    public AnimationCurve horizontalBobCurve;
    [HideInInspector] public float currentBobTime = 0;
    #endregion

    #region Constructors
    public HeadBobbing() { }
    public HeadBobbing(HeadBobbing effectSettings)
    {
        useHeadBob = effectSettings.useHeadBob;
        verticalBobCurve = effectSettings.verticalBobCurve;
        horizontalBobCurve = effectSettings.horizontalBobCurve;
    }
    public HeadBobbing(AnimationCurve headBobCurve) { verticalBobCurve = headBobCurve; }
    public HeadBobbing(AnimationCurve verticalBobCurve, AnimationCurve horizontalBobCurve)
    {
        this.verticalBobCurve = verticalBobCurve;
        this.horizontalBobCurve = horizontalBobCurve;
    }
    #endregion
}

/// <summary>
/// Contains parameters for controlling a First-Person character in which the camera is parented to the character.
/// </summary>
[Serializable]
public class FPLook
{
    #region Variables
    [Header("Components")]
    [HideInInspector] public Camera cam;
    [HideInInspector] public Transform character;

    [Header("Properties")]
    [Range(0, 10)] public float sensativityX = 5;
    [Range(0, 10)] public float sensativityY = 5;
    public float minX = -85;
    public float maxX = 85;
    private bool smooth = false;
    [HideInInspector] public float smoothTime = 5;

    [Header("Values")]
    [HideInInspector] public Vector2 direction = Vector2.zero;
    private Quaternion targetCam = Quaternion.identity;
    private Quaternion targetChar = Quaternion.identity;
    #endregion

    #region Constructors
    public FPLook() { }
    public FPLook(FPLook lookSettings)
    {
        sensativityX = lookSettings.sensativityX;
        sensativityY = lookSettings.sensativityY;
        minX = lookSettings.minX;
        maxX = lookSettings.maxX;
        smooth = lookSettings.smooth;
        smoothTime = lookSettings.smoothTime;
    }
    public FPLook(float sensativityX, float sensativityY, float minimumX = -85, float maximumX = 85, bool smoothLooking = false, float smoothTime = 5)
    {
        this.sensativityX = sensativityX;
        this.sensativityY = sensativityY;
        minX = minimumX;
        maxX = maximumX;
        smooth = smoothLooking;
        this.smoothTime = smoothTime;
    }
    public FPLook(Vector2 sensativityVector, float minimumX = -85, float maximumX = 85, bool smoothLooking = false, float smoothTime = 5)
    {
        sensativityX = sensativityVector.x;
        sensativityY = sensativityVector.y;
        minX = minimumX;
        maxX = maximumX;
        smooth = smoothLooking;
        this.smoothTime = smoothTime;
    }
    #endregion

    #region Facilitators
    // Parts taken from Unity's Standard Asset MouseLook.cs
    public void Look()
    {
        direction.x *= sensativityX;
        direction.y *= sensativityY;
        targetChar *= Quaternion.Euler(0, direction.x, 0);
        targetCam *= Quaternion.Euler(-direction.y, 0, 0);
        targetCam = ClampRotationAroundXAxis(targetCam);
        if (smooth)
        {
            character.localRotation = Quaternion.Slerp(character.localRotation, targetChar, smoothTime * Time.deltaTime);
            cam.transform.localRotation = Quaternion.Slerp(cam.transform.localRotation, targetCam, smoothTime * Time.deltaTime);
        }
        else
        {
            character.localRotation = targetChar;
            cam.transform.localRotation = targetCam;
        }
    }

    // Taken from Unity's Standard Asset MouseLook.cs
    Quaternion ClampRotationAroundXAxis(Quaternion q)
    {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;
        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);
        angleX = Mathf.Clamp(angleX, minX, maxX);
        q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);
        return q;
    }
    #endregion
}