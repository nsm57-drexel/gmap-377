﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class CamGUI : MonoBehaviour
{
    #region Variables
    [SerializeField] private GUIStyle gameOverStyle;
    [SerializeField] private GUIStyle gameOverBetween;
    [SerializeField] private GUIStyle gameOverOutline;
	[SerializeField] private float respawnTime = 10f;
	[SerializeField] private Slider oxySlide;
	[SerializeField] private Slider stunSlide;
	[SerializeField] private GUIStyle winningStyle;
	[SerializeField] private Image[] panel;
	[SerializeField] private Sprite init;
	private float respawnTimer = 0f;
    private bool playerDead = false;
	private PlayerOxygen plOx;
	private StunGun stun;
	private bool win = false;
    #endregion

    #region Facilitators
    private void PlayerKilled() { playerDead = true; }
    #endregion

	private void Awake()
	{
		plOx = gameObject.GetComponentInParent<PlayerOxygen> ();
		stun = gameObject.GetComponentInChildren<StunGun> ();
	}

    #region MonoBehaviors
    private void OnGUI()
    {
        if (playerDead)
        {
            GUI.Label(new Rect(10, 10, Screen.width - 10, Screen.height - 10), "Game Over", gameOverOutline);
            GUI.Label(new Rect(10, 10, Screen.width - 10, Screen.height - 10), "Game Over", gameOverBetween);
            GUI.Label(new Rect(10, 10, Screen.width - 10, Screen.height - 10), "Game Over", gameOverStyle);
            respawnTimer += Time.deltaTime;
            if (respawnTimer >= respawnTime)
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        //else
		oxySlide.value = plOx.currentOxygen;
		if (stun.IsRecharging) {
			stunSlide.value = stun.CurrentRechargeTime;
		} else {
			stunSlide.value = stunSlide.maxValue;
		}
		if (win) {
			GUI.Label(new Rect(10, 10, Screen.width - 10, Screen.height - 10), "You Win!", winningStyle);
		}
    }

	public void Pickup(StoryPickup pick)
	{
		int index = 0;

		foreach (Image i in panel) {
            if (i.sprite == init)
            {
#if UNITY_EDITOR
                index = ArrayUtility.IndexOf(panel, i);
#endif
            }
        }
		panel [index].sprite = pick.img;
	}

	private void Win()
	{
		GameObject robo = GameObject.FindGameObjectWithTag ("Robot");
		robo.GetComponent<NavMeshAgent> ().Stop ();
		win = true;
	}
    #endregion
}
