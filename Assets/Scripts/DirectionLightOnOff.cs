﻿using UnityEngine;
using System.Collections;

public class DirectionLightOnOff : MonoBehaviour
{
    [SerializeField] private Transform robot;
    [SerializeField] private Transform player;
    private Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if (Vector3.Distance(robot.position, player.position) <= 35)
            anim.SetBool("robotNearPlayer", true);
        else
            anim.SetBool("robotNearPlayer", false);
    }
}
