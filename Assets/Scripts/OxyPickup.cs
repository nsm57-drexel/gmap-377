﻿using UnityEngine;

public class OxyPickup : MonoBehaviour
{
    #region Variables
    [Header("Properties")]
    public float maxOxygen = 10;
    [HideInInspector] public float oxygenRemaining = 10;
    #endregion

    #region MonoBehaviors
    private void Update()
    {
        if (oxygenRemaining <= 0)
            Destroy(gameObject);
    }
	public float refill(float curr, float max)
	{
		float need = max - curr, o = oxygenRemaining - need;
		if (o >= 0) {

			oxygenRemaining = o;
			return need;

		} else {
			
			float gib = oxygenRemaining;
			oxygenRemaining = o;
			return gib;

		}
		return 0; //should something go wrong, nothing will happen.
	}
    #endregion
}