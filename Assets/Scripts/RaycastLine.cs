﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(LineRenderer))]
public class RaycastLine : MonoBehaviour
{
    #region Variables
    [Header("Components")]
    private LineRenderer ray;
    private Light rayLight;

    [Header("Properties")]
    [SerializeField] private float rayDistance = 50;
    #endregion

    #region MonoBehaviors
    private void Awake()
    {
        ray = GetComponent<LineRenderer>();
        rayLight = GetComponent<Light>();
    }

    private void Start()
    {
        ray.SetVertexCount(2);
        ray.SetPosition(0, Vector3.zero);
        if (rayLight != null)
            rayLight.range = rayDistance;
    }

    private void Update()
    {
        RaycastHit hitInfo;
        Physics.Raycast(transform.position, transform.forward, out hitInfo, rayDistance, Physics.AllLayers, QueryTriggerInteraction.Ignore);
        if (hitInfo.transform == null)
            ray.SetPosition(1, new Vector3(0, 0, rayDistance));
        else
            ray.SetPosition(1, new Vector3(0, 0, hitInfo.distance));
    }
    #endregion
}